import { Component } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LoadingController, AlertController, Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { TipoServicoProvider } from '../providers/tipo-servico/tipo-servico';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  servico: any;
  servicos: any;


  constructor(
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    private router: Router,
    private platform: Platform,
    public alertController: AlertController,
    private tipoServicoProvider: TipoServicoProvider,
    private toastCtrl: ToastController,
  ) { 
    this.openListTipoServico();
  }

  async doGoogleLogin(){
    const loading = await this.loadingController.create({
      message: 'Aguarde...'
    });
    this.presentLoading(loading);
    this.googlePlus.login({
      'scopes': '', // optional - space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
      'webClientId': environment.googleWebClientId, // optional - clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
      'offline': true, // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      })
      .then(user => {
        //save user data on the native storage
        this.nativeStorage.setItem('google_user', {
          name: user.displayName,
          email: user.email,
          picture: user.imageUrl
        })
        .then(() => {
           this.router.navigate(["/agendamento"]);
        }, (error) => {
          console.log(error);
        })
        loading.dismiss();
      }, err => {
        console.log(err);
        if(!this.platform.is('cordova')){
          this.presentAlert();
        }
        loading.dismiss();
      })
  }

  async doLoginPegandoId(){
    const loading = await this.loadingController.create({
      message: 'Aguarde...'
    });
    this.presentLoading(loading);
    this.nativeStorage.setItem('google_user', {
      name: 'Teste',
      email: 'isoppoLucas@gmail.com',
      picture: 'image',
      id: "get(email)"
    })
    .then(() => {
       this.router.navigate(["/agendamento"]);
    }, (error) => {
      console.log(error);
    })
    loading.dismiss();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
       message: 'Cordova is not available on desktop. Please try this in a real device or in an emulator.',
       buttons: ['OK']
     });

    await alert.present();
  }


  openListTipoServico() {
    this.tipoServicoProvider.getAll()
      .then((result: any) => {
        this.servicos = result;
        console.log(this.servicos);
        //Salvar o token no Ionic Storage para usar em futuras requisições.
        //Redirecionar o usuario para outra tela usando o navCtrl
        //this.navCtrl.pop();
        //this.navCtrl.setRoot()
      })
      .catch((error: any) => {
        console.log(error);
      });
  }

  async presentLoading(loading) {
    return await loading.present();
  }

}
