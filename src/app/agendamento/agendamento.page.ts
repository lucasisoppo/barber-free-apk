import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { LoadingController , ToastController} from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
import { TipoServicoProvider } from './../providers/tipo-servico/tipo-servico';

@Component({
  selector: 'app-agendamento',
  templateUrl: './agendamento.page.html',
  styleUrls: ['./agendamento.page.scss'],
})
export class AgendamentoPage {

  user: any;
  userReady: boolean = false;
  horario: any;
  servico: any;
  servicos: any;
  horarios=[
    {hora:'08:00', value: "0800", disponivel:true},
    {hora:'09:00', value: "0900", disponivel:false},
    {hora:'10:00', value: "1000", disponivel:true},
    {hora:'11:00', value: "1100", disponivel:true},
    {hora:'13:00', value: "1300", disponivel:true},
    {hora:'14:00', value: "1400", disponivel:false},
    {hora:'15:00', value: "1500", disponivel:false},
    {hora:'16:00', value: "1600", disponivel:true},
    {hora:'17:00', value: "1700", disponivel:true},
    {hora:'18:00', value: "1800", disponivel:true},
    {hora:'19:00', value: "1900", disponivel:false}
  ]


  constructor(
    private toast: Toast,
    private toastCtrl: ToastController,
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    private tipoServicoProvider: TipoServicoProvider,
    private router: Router
  ) { 
    this.openListTipoServico();
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Processando...'
    });
     await loading.present();
     this.nativeStorage.getItem('google_user')
    .then(data => {
      this.user = {
        name: data.name,
        email: data.email,
        picture: data.picture,
      };
      this.userReady = true;
      loading.dismiss();
    }, error =>{
      console.log(error);
      loading.dismiss();
    });
  }

  doGoogleLogout(){
    this.googlePlus.logout()
    .then(res => {
      //user logged out so we will remove him from the NativeStorage
      this.nativeStorage.remove('google_user');
      this.router.navigate(["/login"]);
    }, err => {
      console.log(err);
    });
  }

  agendar(){
    this.nativeStorage.getItem('google_user')
    .then(data => {
      this.user = {
        name: data.name,
        email: data.email,
        picture: data.picture,
      };
      this.toast.show(`Agendamento realizado` + this.user.name + this.horario.hora + this.servico.value , '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
      this.userReady = true;
    }, err => {
      console.log(err);
    });
  }

  openListTipoServico() {
    this.tipoServicoProvider.getAll()
      .then((result: any) => {
        this.servicos = result;
        console.log(this.servicos);
        //Caso necessario redirecionar o usuario para outra tela usando o navCtrl
        //this.navCtrl.pop();
        //this.navCtrl.setRoot()
      })
      .catch((error: any) => {
        console.log(error);
      });
  }

}
